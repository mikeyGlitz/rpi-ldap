This project is a fork of https://bitbucket.org/beli-sk/docker-ldap
The Dockerfile has been modified so that the ldap container can run
on a raspberry pi device.


LDAP docker image
=================

Docker image of OpenLDAP server.


Locations
---------

Source of the image is hosted on Bitbucket at
https://bitbucket.org/mikeyGlitz/rpi-ldap

If you find any problems, please post an issue at
https://bitbucket.org/mikeyGlitz/rpi-ldap/issues

The built image is located on Docker Hub at
https://hub.docker.com/r/mkglitz/rpi-ldap

Pull or build
-------------

The image is built automatically on Docker hub in repository **beli/ldap**
and can be pulled using command

    docker pull mkglitz/rpi-ldap

or if you'd prefer to build it yourself from the source repository

    git clone https://bitbucket.org/mikeyGlitz/rpi-ldap.git
    cd rpi-ldap/
    docker build -t mkglitz/rpi-ldap ldap/


Usage
-----

The container requires two volumes: `/config` for the dynamic configuration
database and `/data` for the directory database. The volumes should be owned
and writable by UID 104, GID 107. If not, the container will attempt to change
the ownership to these values and refuses to start if it fails.

If you supply empty configuration volume, it will be initialized with default
values. In this case you need to supply environment variables `CONF_ROOTPW`
for the admin password, either plain text or hashed in an OpenLDAP supported
format and `CONF_BASEDN` for the base of the tree to be served. The default
admin account has RDN `cn=admin` at the base DN you supply and DN
`cn=admin,cn=config` with access to config tree, both with the ROOTPW you
supply.

Please note that if you supply plaintext password, it cannot contain a leading
curly bracket `{` or it will be incorrectly recognized as hashed. A plaintext
password will be hashed before saving.

When you supply an existing configuration, these parameters are ignored and
the configuration is used as is.

The container exposes TCP port 389 for LDAP protocol.

